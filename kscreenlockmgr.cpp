/*
 * Copyright 2011  Alex Merry <alex.merry@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#include "kscreenlockmgr.h"
#include "saverengine.h"

#include <KActionCollection>
#include <KPluginFactory>

K_PLUGIN_FACTORY(KScreenLockMgrFactory,
        registerPlugin<KScreenLockMgr>();
        )
K_EXPORT_PLUGIN(KScreenLockMgrFactory("kscreenlockmgr"))


KScreenLockMgr::KScreenLockMgr(QObject *parent, const QList<QVariant> &)
    : KDEDModule(parent),
      m_engine(new SaverEngine()),
      m_keys(new KActionCollection(this))
{
    m_engine->initShortcuts(m_keys);
}

KScreenLockMgr::~KScreenLockMgr()
{
    delete m_engine;
    delete m_keys;
}

