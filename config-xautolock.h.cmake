/* Define if you have the XScreenSaver extension */
#cmakedefine HAVE_XSCREENSAVER 1

/* Define if you have DPMS support */
#cmakedefine HAVE_DPMS 1
